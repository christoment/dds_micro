/*
 Tugas Akhir Praktikum Embedded
 Jam digital

 Anggota Kelompok :
 Adityo Prabowo 		/ 13211106
 Clement Christopher 	/ 13211138
 Misly Juliani 			/ 13211151

 Spesifikasi :
 */

#include <OsConfig.h>
#include <CoOS.h>
#include <math.h>

#include <stdio.h>
#include "NUC1xx.h"
#include "DrvSYS.h"
#include "DrvGPIO.h"
#include "LCD.h"
#include "Scankey.h"

#include "config.h"
#include "signal.h"
#include "frameBuilder.h"
#include "stateManager.h"

// KEYPAD
#define KEY_AMPLITUDE_UP 2
#define KEY_AMPLITUDE_DOWN 8
#define KEY_SIGNAL_SHIFT_LEFT 4
#define KEY_SIGNAL_SHIFT_RIGHT 6
#define KEY_SIN 1
#define KEY_COS 3
#define KEY_TAN 7
#define KEY_PULSE 9
#define KEY_RESET 5

#define DELAY_HOLD_INCREMENT 0
#define HOLD_COUNTER_THRESHOLD 10

// PRIORITIES
#define PRIORITY_LCD 2
#define PRIORITY_MANAGE_STATE 0
#define PRIORITY_FRAME_GEN 1
#define PRIORITY_READ_ADC 3
#define PRIORITY_KEYPAD_SCAN 3

// RTOS
#define DELAY_KEYPAD_SCAN 	CoTimeDelay(0,0,0,50)
#define DELAY_READ_ADC	 	CoTimeDelay(0,0,0,70)

// TASKS
OS_TID taskFrameGen_TID; // tick frame generation
OS_TID taskKeypadScan_TID; // tick keypad scan
OS_TID taskLCD_TID; // tick display LCD scan
OS_TID taskManageState_TID; // tick state management
OS_TID taskReadADC_TID; // tick read ADC

OS_STK taskFrameGen_stk[128]; // tick frame generation
OS_STK taskKeypadScan_stk[128]; // tick keypad scan
OS_STK taskLCD_stk[128]; // tick display LCD scan
OS_STK taskManageState_stk[128]; // tick state management
OS_STK taskReadADC_stk[128]; // tick read ADC

// AMPLITUDE, FREQ, AND POS COMMAND
typedef enum VarCommand {
	INCREASE, DECREASE, SET, NEUTRAL
} VarCommand;

VarCommand posCommand 		= NEUTRAL;
VarCommand freqCommand 		= NEUTRAL;
VarCommand amplitudeCommand = NEUTRAL;
int posSetVal = 0;
int freqSetVal = 0;
int amplitudeSetVal = 0;

// STATE
int modeCommand = STATE_CONSTANT;

/*** RTOS TASKS ***/
// Timer for interrupt handler
void taskFrameGen(void * ptr) {
	for (;;) {
		generateFrame();

		// awake LCD task
		CoAwakeTask(taskLCD_TID);

		// sleep this task
		CoSuspendTask(taskFrameGen_TID);
	}
}


void taskLCD(void * ptr) {
	StatusType drawLcdOK;
	int i, x, y;
	int prevSignalVal, curSignalVal;

	// Inisialisasi dan clear LCD
	init_LCD();
	DrvGPIO_ClrBit(E_GPD, 14); // backlight LCD on
	clear_LCD();

	for (;;) {
		// clear LCD
		clear_LCD();

		// iterate through all signal
		for (x = SIGNAL_X_MIN; x < SIGNAL_X_MAX; x++) {
			// because x, y is on the top left screen, we need to invert
			// the signal for the result to count from bottom instead of top
			// get and shift the signal to middle of the screen
			curSignalVal = SIGNAL_Y_SHIFT - getSignal(x);

			// draw if the signal is on the frame region
			if ((curSignalVal > SIGNAL_Y_MIN) && (curSignalVal < SIGNAL_Y_MAX)) {
				draw_Pixel(x, curSignalVal, FG_COLOR, BG_COLOR);
			}

			// smoothing part
			if (x > 0) {
				// firstly, check whether the signal is over the window or not
				if (prevSignalVal > SIGNAL_Y_MAX) prevSignalVal = SIGNAL_Y_MAX;
				if (prevSignalVal < SIGNAL_Y_MIN) prevSignalVal = SIGNAL_Y_MIN;
				if (curSignalVal > SIGNAL_Y_MAX) curSignalVal = SIGNAL_Y_MAX;
				if (curSignalVal < SIGNAL_Y_MIN) curSignalVal = SIGNAL_Y_MIN;

				// draw smoothing line
				// tan lines - don't smooth from edge-to-edge condition
				if ((getState() != STATE_TAN) ||
					((getState() == STATE_TAN) && (abs(curSignalVal - prevSignalVal) < SIGNAL_Y_RANGE))) {
					if ((curSignalVal - prevSignalVal) >= 0) {
						for (i = prevSignalVal; i < curSignalVal; i++) {
							draw_Pixel(x, i, FG_COLOR, BG_COLOR);
						}
					} else {
						for (i = prevSignalVal; i > curSignalVal; i--) {
							draw_Pixel(x, i, FG_COLOR, BG_COLOR);
						}
					}
				}
			}

			// draw Y ruler line
			if (x == 0) {
				for (y = 0; y < SIGNAL_Y_MAX; y++) {
					draw_Pixel(x,y,FG_COLOR,BG_COLOR);
				}
			}

			// draw Y ruler point
			if (x == 1) {
				for (y = 16; y < SIGNAL_Y_MAX; y = y + 16) {
					draw_Pixel(x,y,FG_COLOR,BG_COLOR);
				}
			}

			// draw X ruler line
			if (x < SIGNAL_X_MAX) {
				draw_Pixel(x,SIGNAL_Y_MAX-1,FG_COLOR,BG_COLOR);
			}

			// draw X ruler point
			if ((x == 16) || (x == 32) || (x == 48) || (x == 64) || (x == 80) || (x == 96) || (x == 112)) {
				draw_Pixel(x,SIGNAL_Y_MAX-2,FG_COLOR,BG_COLOR);
			}
			prevSignalVal = curSignalVal;
		}

		// suspend LCD draw
		CoSuspendTask(taskLCD_TID);
	}
}

void taskKeypadScan(void * ptr) {
	// Keypad
	unsigned char pressedNumber; // Variabel untuk menyimpan nomor keypad yang ditekan
	unsigned char disablePad = 0;
	unsigned char lastPad = -1; // anti repeating
	unsigned short int holdCounter = 0;
	unsigned short int incrementDelayCounter = 0;

	// Inisialisasi Keypad, menggunakan fungsi yang sudah ada di library utama
	OpenKeyPad(); // initialize 3x3 keypad

	for (;;) {
		// scan keypads - searching for MODE or SET button press
		pressedNumber = ScanKey(); // get pressed keypad index

		if (lastPad != pressedNumber) {
			holdCounter = 0;
			lastPad = pressedNumber;
			disablePad = 0;
			incrementDelayCounter = 0;
		} else if (holdCounter > HOLD_COUNTER_THRESHOLD) {
			if (incrementDelayCounter > DELAY_HOLD_INCREMENT) {
				disablePad = 0;
				incrementDelayCounter = 0;
			} else {
				disablePad = 1;
				incrementDelayCounter++;
			}
		} else {
			holdCounter++;
			disablePad = 1;
		}

		if (!disablePad) {
			switch (pressedNumber) {
			case KEY_AMPLITUDE_DOWN:
				amplitudeCommand = DECREASE;
				break;
			case KEY_AMPLITUDE_UP:
				amplitudeCommand = INCREASE;
				break;
			case KEY_SIGNAL_SHIFT_LEFT:
				posCommand = DECREASE;
				break;
			case KEY_SIGNAL_SHIFT_RIGHT:
				posCommand = INCREASE;
				break;
			case KEY_COS:
				modeCommand = STATE_COS;
				break;
			case KEY_SIN:
				modeCommand = STATE_SIN;
				break;
			case KEY_TAN:
				modeCommand = STATE_TAN;
				break;
			case KEY_PULSE:
				modeCommand = STATE_PULSE;
				break;
			case KEY_RESET:
				amplitudeCommand = SET;
				amplitudeSetVal = 1;
				posCommand = SET;
				posSetVal = 0;
				modeCommand = STATE_SIN;
				break;
			}

			// awake state manager task
			CoAwakeTask(taskManageState_TID);
		}

		DELAY_KEYPAD_SCAN;
	}
}

void taskManageState(void * ptr) {
	for (;;) {
		// check amplitude, freq, pos, and mode

		// check mode first
		switch (modeCommand) {
		case STATE_COS :
			inputCosExecuted();
			break;
		case STATE_SIN :
			inputSinExecuted();
			break;
		case STATE_TAN :
			inputTanExecuted();
			break;
		case STATE_PULSE :
			inputPulseExecuted();
			break;
		case STATE_CONSTANT: // no change
			break;
		}

		// check amplitude
		switch (amplitudeCommand) {
		case INCREASE :
			stepIncreaseAmplitude();
			break;
		case DECREASE :
			stepDecreaseAmplitude();
			break;
		case SET :
			setAmplitude(amplitudeSetVal);
			break;
		}

		// check pos
		switch (posCommand) {
		case INCREASE :
			stepIncreasePos();
			break;
		case DECREASE :
			stepDecreasePos();
			break;
		case SET :
			setPos(posSetVal);
			break;
		}

		// check frequency
		switch (freqCommand) {
		case SET :
			setFrequency(freqSetVal);
			break;
		}

		// turn off command
		amplitudeCommand = NEUTRAL;
		posCommand = NEUTRAL;
		freqCommand = NEUTRAL;
		modeCommand = STATE_CONSTANT;

		// awake frame generation
		CoAwakeTask(taskFrameGen_TID);

		// pause this task
		CoSuspendTask(taskManageState_TID);
	}
}

void taskReadADC(void) {
	int adcReadVal = 0;

	GPIOA->OFFD |= 0x00800000; //Disable digital input path
	SYS->GPAMFP.ADC7_SS21_AD6 = 1; //Set ADC function

	SYSCLK->CLKSEL1.ADC_S = 2; //Select 22Mhz for ADC
	SYSCLK->CLKDIV.ADC_N = 1; //ADC clock source = 22Mhz/2 =11Mhz;
	SYSCLK->APBCLK.ADC_EN = 1; //Enable clock source
	ADC->ADCR.ADEN = 1; //Enable ADC module

	ADC->ADCR.DIFFEN = 0; //single end input
	ADC->ADCR.ADMD = 0; //single mode

	ADC->ADCHER.CHEN = 0x80;

	ADC->ADSR.ADF = 1; //clear the A/D interrupt flags for safe
	ADC->ADCR.ADST=1;

	for (;;) {
		if (ADC->ADSR.ADF == 1) { // conversion is done
			ADC->ADSR.ADF = 1; // write 1 to clear the flag
			adcReadVal = ADC->ADDR[7].RSLT; // read ADC value

			// update freq
			freqSetVal = adcReadVal;
			freqCommand = SET;

			ADC->ADCR.ADST = 1; // restart ADC sample

			// awake state manager task
			CoAwakeTask(taskManageState_TID);
		}


		DELAY_READ_ADC;
	}
}

void initSignalState() {
	amplitudeCommand = SET;
	amplitudeSetVal = AMPLITUDE_DEFAULT_VALUE;

	posCommand = SET;
	posSetVal = POS_DEFAULT_VALUE;

	modeCommand = STATE_SIN;
}

int main(void) {
	// Set clock yang digunakan sistem
	UNLOCKREG();
	DrvSYS_Open(50000000); // set MCU to run at 50MHz
	LOCKREG();

	CoInitOS(); // init CoOS

	initSignalState();

	taskFrameGen_TID = CoCreateTask(taskFrameGen, 0, PRIORITY_FRAME_GEN, &taskFrameGen_stk[128-1], 128);
	taskKeypadScan_TID = CoCreateTask(taskKeypadScan, 0, PRIORITY_KEYPAD_SCAN, &taskKeypadScan_stk[128-1], 128);
	taskLCD_TID = CoCreateTask(taskLCD, 0, PRIORITY_LCD, &taskLCD_stk[128-1], 128);
	taskManageState_TID = CoCreateTask(taskManageState, PRIORITY_MANAGE_STATE, 2, &taskManageState_stk[128-1], 128);
	taskReadADC_TID = CoCreateTask(taskReadADC, 0, PRIORITY_READ_ADC, &taskReadADC_stk[128-1], 128);

	CoStartOS();

	// prevention?
	while (1) {
	}
}

