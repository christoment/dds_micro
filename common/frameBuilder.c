#include "frameBuilder.h"

void generateFrame() {
    // sweep across X_MIN to X_MAX
    int freq = getFrequency();
    int pos = getPos();
    int amplitude = getAmplitude();

    double generatedValue;
    int i;
    
    for (i = SIGNAL_X_MIN; i < SIGNAL_X_MAX; i++) {
        generatedValue = generateWave(getState(), i + pos, freq, amplitude);
        putSignal(i, generatedValue);
    }
}
