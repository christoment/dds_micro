/* 
 * File:   freq.h
 * Author: Christ
 * 
 * mengatur perubahan frequensi
 * Created on 1 December 2014, 8:03 PM
 */

#ifndef FREQ_H
#define	FREQ_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "config.h"
    
    /**
     * Mengatur frekuensi menjadi yang disimpan
     * @param freq frekuensi dalam integer
     */
    void setFrequency(int freq);

    /**
     * Mengambil frekuensi yang disimpan
     * @return integer frekuensi
     */
    int getFrequency();



#ifdef	__cplusplus
}
#endif

#endif	/* FREQ_H */

