/* 
 * File:   config.h
 * Author: Christ
 *
 * Created on December 2, 2014, 1:27 PM
 */

#ifndef CONFIG_H
#define	CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "math.h"

/// STATE CONSTANT
#define STATE_SIN 0
#define STATE_COS 1
#define STATE_TAN 2
#define STATE_PULSE 3
#define STATE_CONSTANT -1
    
/// SIGNAL GEOMETRICS
#define FREQ_MAX 4096
#define FREQ_MIN 0
#define FREQ_SCALE 10/FREQ_MAX
    
#define POS_MAX 1000
#define POS_MIN 0
#define POS_STEP_INC 1
#define POS_STEP_DEC 1
    
#define AMPLITUDE_MIN -32
#define AMPLITUDE_MAX 32
#define AMPLITUDE_STEP_INC 1
#define AMPLITUDE_STEP_DEC 1
#define AMPLITUDE_DEFAULT_VALUE 5

#define POS_DEFAULT_VALUE 0

#define TANGENT_AMPLITUDE 15
    
/// DRAWABLE AREA
#define SIGNAL_X_MIN 0
#define SIGNAL_X_MAX 128
#define SIGNAL_Y_MIN 0
#define SIGNAL_Y_MAX 64
#define SIGNAL_Y_RANGE SIGNAL_Y_MAX - SIGNAL_Y_MIN

#define SIGNAL_Y_SCALE 1
#define SIGNAL_Y_SHIFT (SIGNAL_Y_MAX + SIGNAL_Y_MIN) / 2
#define SIGNAL_X_SCALE 100
#define PI 3.1416


#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

