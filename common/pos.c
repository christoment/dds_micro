#include "pos.h"

static int sPos;

void setPos(int x) {
    // cek kondisi batas
    if (x > POS_MAX)
        x = POS_MAX;
    else if (x < POS_MIN)
        x = POS_MIN;

    sPos = x;
}

int getPos() {
    return sPos;
}

void stepIncreasePos() {
    if (sPos < POS_MAX) {
        sPos += POS_STEP_INC;
    }
}

void stepDecreasePos() {
    if (sPos > POS_MIN) {
        sPos -= POS_STEP_DEC;
    }
}
