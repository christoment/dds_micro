/* 
 * File:   signal.h
 * Author: Christ
 *
 * Mengatur signal yang dibentuk
 * Created on 1 December 2014, 8:14 PM
 */

#ifndef SIGNAL_H
#define	SIGNAL_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "config.h"
    
    /**
     * Put a signal Y on location X
     * @param x integer position
     * @param y integer signal
     */
    void putSignal(int x, int y);
    
    /**
     * Get a signal from location X
     * @param x integer location
     * @return signal on X
     */
    int getSignal(int x);
    
    /**
     * Get a shifted signal from location X
     * @param x location
     * @return shifted signal on X
     */
    int getShiftedSignal (int x);
    
    /**
     * Initialize signal memory
     */
    void initializeSignalMem();


#ifdef	__cplusplus
}
#endif

#endif	/* SIGNAL_H */

