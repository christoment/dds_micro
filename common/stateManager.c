#include "stateManager.h"

#define SIN_INPUT 1,0,0,0
#define COS_INPUT 0,1,0,0
#define TAN_INPUT 0,0,1,0
#define PULSE_INPUT 0,0,0,1

static int sFsmState;

void inputSinExecuted() {
    executeFsm(SIN_INPUT, &sFsmState);
}

void inputCosExecuted() {
    executeFsm(COS_INPUT, &sFsmState);
}

void inputTanExecuted() {
    executeFsm(TAN_INPUT, &sFsmState);
}

void inputPulseExecuted() {
    executeFsm(PULSE_INPUT, &sFsmState);
}

int getState() {
    return sFsmState;
}