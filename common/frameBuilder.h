/* 
 * File:   frameBuilder.h
 * Author: Christ
 *
 * Mengatur pembuatan satu frame yang berisi sinyal
 * Created on 1 December 2014, 8:45 PM
 */

#ifndef FRAMEBUILDER_H
#define	FRAMEBUILDER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "signal.h"
#include "stateManager.h"
#include "waveBuilder.h"
#include "freq.h"
#include "pos.h"
#include "amplitude.h"

#include "config.h"

    /**
     * Menghitung dan generasi dari sinyal pada state tertentu
     */
    void generateFrame();

#ifdef	__cplusplus
}
#endif

#endif	/* FRAMEBUILDER_H */

