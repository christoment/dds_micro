/* 
 * File:   fsm.h
 * Author: Christ
 *
 * Created on 1 December 2014, 7:46 PM
 */

#ifndef FSM_H
#define	FSM_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "config.h"

    /**
     * Eksekusi fsm berdasarkan input
     * @param btn_sin sin button, active high
     * @param btn_cos cos button, active high 
     * @param btn_tan tan button, active high 
     * @param btn_pulse pulse button, active high
     * @param state input and output state
     */
    void executeFsm(int btn_sin, int btn_cos, int btn_tan, int btn_pulse, int *state);


#ifdef	__cplusplus
}
#endif

#endif	/* FSM_H */

