#include "amplitude.h"

static int sAmplitude = AMPLITUDE_MIN;

void setAmplitude(int y) {
    if (y > AMPLITUDE_MAX)
        y = AMPLITUDE_MAX;
    else if (y < AMPLITUDE_MIN)
        y = AMPLITUDE_MIN;

    sAmplitude = y;
}

int getAmplitude() {
    return sAmplitude;
}

void stepIncreaseAmplitude() {
    if (sAmplitude < AMPLITUDE_MAX) {
        sAmplitude += AMPLITUDE_STEP_INC;
    } else {
    	sAmplitude = AMPLITUDE_MAX;
    }
}

void stepDecreaseAmplitude() {
    if (sAmplitude > AMPLITUDE_MIN) {
        sAmplitude -= AMPLITUDE_STEP_DEC;
    } else {
    	sAmplitude = AMPLITUDE_MIN;
    }
}
