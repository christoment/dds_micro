
#include "fsm.h"
// file ini berisi FSM yang akan diuji
// state_counter param


/**
 * Eksekusi fsm berdasarkan input
 * @param btn_sin sin button, active high
 * @param btn_cos cos button, active high 
 * @param btn_tan tan button, active high 
 * @param btn_pulse pulse button, active high
 * @param state input and output state
 */
void executeFsm(int btn_sin, int btn_cos, int btn_tan, int btn_pulse, int *state) {
    switch (*state) {
        case STATE_SIN:
            if (btn_cos) {
                *state = STATE_COS;
            } else if (btn_tan) {
                *state = STATE_TAN;
            } else if (btn_pulse) {
                *state = STATE_PULSE;
            }
            break;
        case STATE_COS:
            if (btn_sin) {
                *state = STATE_SIN;
            } else if (btn_tan) {
                *state = STATE_TAN;
            } else if (btn_pulse) {
                *state = STATE_PULSE;
            }
            break;
        case STATE_TAN:
            if (btn_sin) {
                *state = STATE_SIN;
            } else if (btn_cos) {
                *state = STATE_COS;
            } else if (btn_pulse) {
                *state = STATE_PULSE;
            }
            break;
        case STATE_PULSE:
            if (btn_sin) {
                *state = STATE_SIN;
            } else if (btn_cos) {
                *state = STATE_COS;
            } else if (btn_tan) {
                *state = STATE_TAN;
            }
            break;
    }
}