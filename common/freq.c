#include "freq.h"

static int sFreq = FREQ_MIN;

void setFrequency(int freq) {
    if (freq > FREQ_MAX) {
        freq = FREQ_MAX;
    } else if (freq < FREQ_MIN) {
        freq = FREQ_MIN;
    }
    
    sFreq = freq;
}

int getFrequency() {
    return sFreq;
}
