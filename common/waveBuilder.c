#include "waveBuilder.h"

double getSinVal(double x, double f) {
    return sin(f * x * PI * 2) * SIGNAL_Y_SCALE;
}

double getCosVal(double x, double f) {
    return cos(f * x * PI * 2) * SIGNAL_Y_SCALE;
}

double getTanVal(double x, double f) {
	return tan(f * x * PI * 2) * SIGNAL_Y_SCALE;
}

double getPulseVal(double x, double f) {
	double period = 1/f;
	int    divisor = floor(x / period);
	double remainder = x - (divisor * period);

    if (remainder > period/2) {
    	// positive direction, high pulse
        return 1 * SIGNAL_Y_SCALE;
    } else {
        return -1 * SIGNAL_Y_SCALE;
    }
}

double generateWave(int state, int x, double f, int a) {
    switch (state) {
        case STATE_SIN:
            return a * getSinVal((double)x / SIGNAL_X_SCALE, f * FREQ_SCALE);
            break;
        case STATE_COS:
            return a * getCosVal((double)x / SIGNAL_X_SCALE, f * FREQ_SCALE);
            break;
        case STATE_TAN:
        	// we don't get to specify tangent amplitude. use fixed value until this are solved
//            return a * getTanVal((double)x / SIGNAL_X_SCALE, f * FREQ_SCALE);
            return TANGENT_AMPLITUDE * getTanVal((double)x / SIGNAL_X_SCALE, f * FREQ_SCALE);
            break;
        case STATE_PULSE:
            return a * getPulseVal((double)x / SIGNAL_X_SCALE, f * FREQ_SCALE);
            break;
    }
}
