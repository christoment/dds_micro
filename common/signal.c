#include "signal.h"

static int sSignalMem[SIGNAL_X_MAX - SIGNAL_X_MIN + 1];

void putSignal(int x, int y) {
    // cek kondisi batas
    if (x > SIGNAL_X_MAX)
        x = SIGNAL_X_MAX;
    else if (x < SIGNAL_X_MIN)
        x = SIGNAL_X_MIN;

    if (y > AMPLITUDE_MAX)
        y = AMPLITUDE_MAX;
    else if (y < AMPLITUDE_MIN)
        y = AMPLITUDE_MIN;

    sSignalMem[x] = y;
}

int getSignal(int x) {
    return sSignalMem[x];
}

int getShiftedSignal (int x) {
    return sSignalMem[x] + SIGNAL_Y_SHIFT;
}
    
void initializeSignalMem() {
    int i;
    for (SIGNAL_X_MIN; i < SIGNAL_X_MAX; i++) {
        sSignalMem[i] = AMPLITUDE_MIN;
    }
}
