/* 
 * File:   pos.h
 * Author: Christ
 *
 * mengatur perubahan posisi sinyal
 * Created on 1 December 2014, 8:09 PM
 */

#ifndef POS_H
#define	POS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "config.h"

    /**
     * Mengatur posisi yang disimpan
     * @param x integer posisi
     */
    void setPos(int x);

    /**
     * Mengambil posisi yang disimpan
     * @return integer posisi
     */
    int getPos();

    /**
     * Meningkatkan posisi sebanyak satu step (ke kanan)
     */
    void stepIncreasePos();

    /**
     * Mengurangi posisi sebanyak satu step (ke kiri)
     */
    void stepDecreasePos();


#ifdef	__cplusplus
}
#endif

#endif	/* POS_H */

