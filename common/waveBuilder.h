/* 
 * File:   waveBuilder.h
 * Author: Christ
 *
 * Pembentuk signal, bergantung pada state
 * Created on 1 December 2014, 8:36 PM
 */

#ifndef WAVEBUILDER_H
#define	WAVEBUILDER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <math.h>
#include "signal.h"
#include "fsm.h"
    
    /**
     * Menghasilkan gelombang pada posisi X dengan jenis sinyal seperti 
     * pada state
     * 
     * @param state jenis sinyal
     * @param x posisi
     * @param f frekuensi
     * @param a amplitudo
     * @return sinyal keluaran
     */
	double generateWave(int state, int x, double f, int a);


#ifdef	__cplusplus
}
#endif

#endif	/* WAVEBUILDER_H */
