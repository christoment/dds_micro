/* 
 * File:   amplitude.h
 * Author: Christ
 *
 * Mengatur amplitudo dari sinyal
 * Created on 1 December 2014, 8:12 PM
 */

#ifndef AMPLITUDE_H
#define	AMPLITUDE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "config.h"

    /**
     * Mengatur amplitudo sinyal yang disimpan
     * @param y integer amplitudo sinyal
     */
    void setAmplitude(int y);

    /**
     * Mengambil amplitudo sinyal yang disimpan
     * @return integer amplitudo sinyal
     */
    int getAmplitude();

    /**
     * Meningkatkan amplitudo sinyal sebanyak satu step
     */
    void stepIncreaseAmplitude();

    /**
     * Mengurangi amplitudo sinyal sebanyak satu step
     */
    void stepDecreaseAmplitude();


#ifdef	__cplusplus
}
#endif

#endif	/* AMPLITUDE_H */

