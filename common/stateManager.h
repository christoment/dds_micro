/* 
 * File:   stateManager.h
 * Author: Christ
 *
 * Mengatur perubahan state berdasarkan input
 * Created on 1 December 2014, 8:38 PM
 */

#ifndef STATEMANAGER_H
#define	STATEMANAGER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "fsm.h"

    /**
     * Input sinus dieksekusi
     */
    void inputSinExecuted();
    
    /**
     * Input cosinus dieksekusi
     */
    void inputCosExecuted();
    
    /**
     * Input tangent dieksekusi
     */
    void inputTanExecuted();
    
    /**
     * Input pulse dieksekusi
     */
    void inputPulseExecuted();

    /**
     * Mengembalikan state mesin sekarang
     * @return State mesin
     */
    int getState();


#ifdef	__cplusplus
}
#endif

#endif	/* STATEMANAGER_H */

