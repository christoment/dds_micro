/* 
 * File:   main.c
 * Author: Christ
 *
 * Created on 1 December 2014, 7:53 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "amplitude.h"
#include "freq.h"
#include "fsm.h"
#include "pos.h"
#include "signal.h"
#include "waveBuilder.h"
#include "stateManager.h"
#include "frameBuilder.h"

#define MAGIC_TEST_NUMBER 0
#define AMPLITUDE_MID (AMPLITUDE_MAX + AMPLITUDE_MIN) / 2
#define FREQ_MID (FREQ_MAX + FREQ_MIN) / 2
#define POS_MID (POS_MAX + POS_MIN) / 2
#define SIGNAL_MID (AMPLITUDE_MAX + AMPLITUDE_MIN) / 2

static int errorCount = 0;
static int counter = 0;

void simpleAssert(int condition) {
    if (condition) {
        printf("TEST %d - OK\n", counter);
    } else {
        printf("TEST %d - FAIL\n", counter);
        errorCount += 1;
    }
    counter++;
}

void testAmplitude() {
    int maxVal = AMPLITUDE_MAX;
    int minVal = AMPLITUDE_MIN;
    int compare = AMPLITUDE_MID;
    setAmplitude(compare);
    simpleAssert(getAmplitude() == compare);

    compare = minVal;
    setAmplitude(compare);
    simpleAssert(getAmplitude() == compare);

    compare = maxVal;
    setAmplitude(compare);
    simpleAssert(getAmplitude() == compare);

    compare = minVal - 1;
    setAmplitude(compare);
    simpleAssert(getAmplitude() == minVal);

    compare = maxVal + 1;
    setAmplitude(compare);
    simpleAssert(getAmplitude() == maxVal);

    setAmplitude(AMPLITUDE_MID);
    stepIncreaseAmplitude();
    simpleAssert(getAmplitude() == AMPLITUDE_MID + AMPLITUDE_STEP_INC);

    setAmplitude(AMPLITUDE_MID);
    stepDecreaseAmplitude();
    simpleAssert(getAmplitude() == AMPLITUDE_MID - AMPLITUDE_STEP_DEC);
}

void testFreq() {
    int maxVal = FREQ_MAX;
    int minVal = FREQ_MIN;
    int compare = FREQ_MID;
    setFrequency(compare);
    simpleAssert(getFrequency() == compare);

    compare = minVal;
    setFrequency(compare);
    simpleAssert(getFrequency() == compare);

    compare = maxVal;
    setFrequency(compare);
    simpleAssert(getFrequency() == compare);

    compare = minVal - 1;
    setFrequency(compare);
    simpleAssert(getFrequency() == minVal);

    compare = maxVal + 1;
    setFrequency(compare);
    simpleAssert(getFrequency() == maxVal);
}

void testPos() {
    int maxVal = POS_MAX;
    int minVal = POS_MIN;
    int compare = POS_MID;
    setPos(compare);
    simpleAssert(getPos() == compare);

    compare = minVal;
    setPos(compare);
    simpleAssert(getPos() == compare);

    compare = maxVal;
    setPos(compare);
    simpleAssert(getPos() == compare);

    compare = minVal - 1;
    setPos(compare);
    simpleAssert(getPos() == minVal);

    compare = maxVal + 1;
    setPos(compare);
    simpleAssert(getPos() == maxVal);

    setPos(POS_MID);
    stepIncreasePos();
    simpleAssert(getPos() == POS_MID + POS_STEP_INC);

    setPos(POS_MID);
    stepDecreasePos();
    simpleAssert(getPos() == POS_MID - POS_STEP_DEC);
}

void testSignal() {
    int minVal = AMPLITUDE_MIN;
    int maxVal = AMPLITUDE_MAX;
    int testMax = SIGNAL_X_MAX;
    int testMin = SIGNAL_X_MIN;
    int yCompare = SIGNAL_MID;
    int xCompare = testMin;

    initializeSignalMem();

    // single test
    putSignal(xCompare, yCompare);
    simpleAssert(getSignal(xCompare) == yCompare);

    // boundary test
    xCompare = testMin;
    yCompare = minVal;
    putSignal(xCompare, yCompare);
    simpleAssert(getSignal(xCompare) == yCompare);

    xCompare = testMax;
    yCompare = minVal;
    putSignal(xCompare, yCompare);
    simpleAssert(getSignal(xCompare) == yCompare);

    xCompare = testMin;
    yCompare = minVal;
    putSignal(xCompare - 1, yCompare);
    simpleAssert(getSignal(xCompare) == yCompare);

    xCompare = testMax;
    yCompare = minVal;
    putSignal(xCompare + 1, yCompare);
    simpleAssert(getSignal(xCompare) == yCompare);

    xCompare = testMin;
    yCompare = minVal;
    putSignal(xCompare, yCompare - 1);
    simpleAssert(getSignal(xCompare) == yCompare);

    xCompare = testMin;
    yCompare = maxVal;
    putSignal(xCompare, yCompare + 1);
    simpleAssert(getSignal(xCompare) == yCompare);
}

void testWaveBuilder() {
    int i;

    int DELTA = SIGNAL_X_SCALE / 8;
    int TEST_COUNT = 2 * SIGNAL_X_SCALE / DELTA;
    static const double FREQ = 1;
    static const int AMPLITUDE = 1;
    int result;
    int state;

    initializeSignalMem();

    // sine wave test
    printf("SINE TEST\n");
    state = STATE_SIN;

    for (i = 0; i < TEST_COUNT; i++) {
        //        result[i] = generateWave(state, testFreq[i], FREQ);
        result = generateWave(state, i * DELTA, FREQ, AMPLITUDE);
        printf("-> %d\n", result);
    }

    // cos wave test
    printf("COS TEST\n");
    state = STATE_COS;

    for (i = 0; i < TEST_COUNT; i++) {
        result = generateWave(state, i * DELTA, FREQ, AMPLITUDE);
        printf("-> %d\n", result);
    }

    // tan wave test
    printf("TAN TEST\n");
    state = STATE_TAN;

    for (i = 0; i < TEST_COUNT; i++) {
        result = generateWave(state, i * DELTA, FREQ, AMPLITUDE);
        printf("-> %d\n", result);
    }

    // pulse wave test
    printf("PULSE TEST\n");
    state = STATE_PULSE;

    for (i = 0; i < TEST_COUNT; i++) {
        result = generateWave(state, i * DELTA, FREQ, AMPLITUDE);
        printf("-> %d\n", result);
    }
}

void testFrameBuilder() {
    int i;
    int i2;
    int i_buff;

    static const int POS = 0;
    static const double FREQ = 1;
    static const int AMPLITUDE = 1;
    static const int BUFFER_SIZE = 8;
    int result[8];

    // initialization
    initializeSignalMem();
    setPos(POS);
    setFrequency(FREQ);
    setAmplitude(AMPLITUDE);
    inputSinExecuted();

    // sine wave test
    printf("FRAME SINE TEST\n");
    generateFrame();

    i_buff = 0;
    for (i = SIGNAL_X_MIN; i < SIGNAL_X_MAX; i++) {
        if (i_buff >= BUFFER_SIZE) {
            i_buff = 0;
            printf("->");
            for (i2 = 0; i2 < BUFFER_SIZE; i2++) {
                printf("%d ", result[i2]);
            }
            printf("\n");
        }
        
        result[i_buff++] = getSignal(i);
    }
    
    // frame sine shifted test
    printf("FRAME SHIFTED SINE TEST\n");
    generateFrame();

    i_buff = 0;
    for (i = SIGNAL_X_MIN; i < SIGNAL_X_MAX; i++) {
        if (i_buff >= BUFFER_SIZE) {
            i_buff = 0;
            printf("->");
            for (i2 = 0; i2 < BUFFER_SIZE; i2++) {
                printf("%d ", result[i2]);
            }
            printf("\n");
        }
        
        result[i_buff++] = getShiftedSignal(i);
    }
}

int main() {
    printf("Test Amplitude =====\n");
    errorCount = 0;
    counter = 0;
    testAmplitude();

    printf("Test Frequency =====\n");
    errorCount = 0;
    counter = 0;
    testFreq();

    printf("Test Pos =====\n");
    errorCount = 0;
    counter = 0;
    testPos();

    printf("Test Signal =====\n");
    errorCount = 0;
    counter = 0;
    testSignal();

    printf("Test WaveBuilder =====\n");
    errorCount = 0;
    counter = 0;
    testWaveBuilder();

    printf("Test FrameBuilder =====\n");
    errorCount = 0;
    counter = 0;
    testFrameBuilder();

    printf("Error Count : %d", errorCount);
    return 0;
}


